Group: group 39

Question
========

RQ: Is there a correlation between the death rate and birth rate of Russia?

Null hypothesis: There is no correlation between the death rate and birth rate of Russia.

Alternative hypothesis: There is a correlation between the death rate and birth rate of Russia.

Dataset
=======

URL: https://www.kaggle.com/dwdkills/russian-demography

Column Headings:

```
> read.csv("russian_demography.csv")
> colnames(russian_demography)
[1] "year"         "region"       "npg"          "birth_rate"   "death_rate"  
[6] "gdw"          "urbanization"

```